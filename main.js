/*
    Task 1:

    Напишите результат сравнения рядом с выражением в виде комментария.
    Необходимо выполнить это задание без использования console.log()
    Данные для сравнения:

    "42" == 42              //true
    "0" == 0                //true
    "0" == false            //true
    "true" == true          //false
    true == (1 == "1")      //true

    "42" === 42             //false
    "0" === 0               //false
    "0" === false           //false
    "true" === true         //false
    true === (1 === "1")    //false
*/





/* 
    Task 2:

    Переменная a = 'foo' , а переменная b = 'bar'
    Чему будет равен результат выражения: a+ +b 
    
    ps: два плюса - это не опечатка

    a+ +b = fooNan;

*/




/*
    Task 3:

    Напишите результат сравнения следующих выражений :

    "ананас" > "яблоко"         //false
    undefined == null           //true
    undefined === null          //false

    Необходимо выполнить это задание без использования console.log()

*/

/*
    Task 4:

    Создайте свою конструкцию УСЛОВИЕ
    Минимальное количество условий в одной конструкции: 5

*/

let price = 150;
if (price < 100 ) {
    console.log('Too cheap');
} else if (price > 100 && price < 200) {
    console.log('Still not enought');
} else if (price > 150 && price < 200) {
    console.log('Little bit more((;');
} else if (price == 200) {
    console.log('Exatly what i want');
} else if (price > 200) {
    console.log('Too expensive');
} else console.log('Try again');

/*
    Task 5:

    Созданную вами конструкцию из задания номер 4, запишите с помощью тернарного оператора

*/


(price < 100) ? console.log('Too cheap') :
(price > 100) && price < 200 ? console.log('Still not enought') :
(price > 150) && price < 200 ? console.log('Little bit more((;') :
(price == 200) ? console.log('Exatly what i want') : 
(price > 200) ? console.log('Too expensive') : console.log('Try again');
/*
    Task 6:
    Загадайте 5 чисел от 1 до 100, 
    а затем запросите у пользователя одно число в этом диапазоне.
    При КАЖДОМ совпадении числа выводите сообщение в МОДАЛЬНОМ окне: 
    "Вы угадали число". Если пользователь не угадал, то выводите сообщение:
    "Вы не угадали. Попробуйте ещё раз!"

    В задании необходимо использовать конструкцию switch/case.
*/

let num1 = 5,
    num2 = 7,
    num3 = 32,
    num4 = 47,
    num5 = 99;
let answere = +prompt('Попробуй угадать число от 1 до 100');
console.log(answere)
switch (answere) {
    case(num1) :
        alert('Вы угадали число');       
        break;
    case(num2) :
        alert('Вы угадали число');       
        break;
    case(num3) :
        alert('Вы угадали число');       
        break;
    case(num4) :
        alert('Вы угадали число');       
        break;
    case(num5) :
        alert('Вы угадали число');       
        break;
    default :
        alert('Вы не угадали. Попробуйте ещё раз!');
        +prompt('Попробуй угадать число от 1 до 100')
}
    
